// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidsystems
 * \brief Fluidsystem specialitites for radioactive substances.
 */
#ifndef DUMUX_RADIOACTIVE_FLUID_SYSTEM_HH
#define DUMUX_RADIOACTIVE_FLUID_SYSTEM_HH

#include <dune/common/exceptions.hh>

#include <dumux/common/typetraits/typetraits.hh>

namespace Dumux {
namespace Fluidsystems {

/*!
 * \ingroup Fluidsystems
 * \brief Fluidsystem specialitites for radioactive substances.
 */
template<class Scalar, class Implementation>
class Radioactive
{
public:
    /*!
     * \brief Calculation of Becquerel per mass of a radioactive component
     */
    static Scalar specificActivityTheo(int compIdx, const Scalar halfLife)
    {
        return Implementation::specificActivityTheo(compIdx, halfLife);
    }

    /*!
     * \brief Calculation of decay rate of a component
     */
    static Scalar decayRate(int compIdx, const Scalar halfLife)
    {
        return Implementation::decayRate(compIdx, halfLife);
    }
};

} // end namespace Components
} // end namespace Dumux

#endif
