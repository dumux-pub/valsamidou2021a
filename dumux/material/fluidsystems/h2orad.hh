// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidsystems
 * \brief A fluid system for H2O with dissolved Radioactive Ions.
 */
#ifndef DUMUX_H2ORAD_ONEPHASE_FLUID_SYSTEM_HH
#define DUMUX_H2ORAD_ONEPHASE_FLUID_SYSTEM_HH

#include <math.h>
#include <dumux/material/fluidsystems/base.hh>
#include <dumux/material/fluidsystems/radioactive.hh>

#include <dumux/material/constants.hh>
#include <dumux/material/components/tabulatedcomponent.hh>

#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/radioactive/caesium.hh>
#include <dumux/material/components/radioactive/iod.hh>
#include <dumux/material/components/radioactive/strontium.hh>
#include <dumux/material/components/radioactive/uran.hh>

#include <dumux/common/exceptions.hh>

#include <dumux/io/name.hh>

namespace Dumux {
namespace FluidSystems {

/*!
 * \ingroup Fluidsystems
 * \brief A compositional single phase fluid system consisting of
 *        water with multiple radioactive components.
 */
template< class Scalar, 
          class H2OType = Components::TabulatedComponent<Dumux::Components::H2O<Scalar>> >
class H2ORad 
: public Base< Scalar, H2ORad<Scalar, H2OType>>
{
    using ThisType = H2ORad<Scalar, H2OType>;
    using Base = Dumux::FluidSystems::Base<Scalar, ThisType>;

public:
    //! export the involved components
    using H2O = H2OType;
    using Caesium137 = Dumux::Components::Caesium137<Scalar>;
    using Iod129 = Dumux::Components::Iod129<Scalar>;
    using Strontium90 = Dumux::Components::Strontium90<Scalar>;
    using Uran235 = Dumux::Components::Uran235<Scalar>;

    static const int numPhases = 1;     //!< Number of phases in the fluid system

    static constexpr int phase0Idx = 0; //!< Index of the first (and only) phase
    static constexpr int liquidPhaseIdx = phase0Idx; //!< The one considered phase is liquid

    static const int numComponents = 5; //!< Number of components in the fluid system (H2O, RadIons)

    //all component indices
    static constexpr int H2OIdx = 0;
    static constexpr int Caesium137Idx = 1;
    static constexpr int Iod129Idx = 2;
    static constexpr int Strontium90Idx = 3;
    static constexpr int Uran235Idx = 4;

    static constexpr int comp0Idx = H2OIdx;
    static constexpr int comp1Idx = Caesium137Idx;
    static constexpr int comp2Idx = Iod129Idx;
    static constexpr int comp3Idx = Strontium90Idx; 
    static constexpr int comp4Idx = Uran235Idx;

    /*!
     * \brief Return the human readable name of the phase
     * \param phaseIdx The index of the fluid phase to consider
     */
    static const std::string phaseName(int phaseIdx = liquidPhaseIdx)
    {
        assert(phaseIdx == liquidPhaseIdx);
        return IOName::liquidPhase();
    }

    /*!
     * \brief Returns whether the fluids are miscible
     * \note There is only one phase, so miscibility makes no sense here
     */
    static constexpr bool isMiscible()
    {
        return false;
    }

    /*!
     * \brief Return whether a phase is gaseous
     * \param phaseIdx The index of the fluid phase to consider
     */
    static constexpr bool isGas(int phaseIdx = liquidPhaseIdx)
    {
        assert(phaseIdx == liquidPhaseIdx);
        return false;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal mixture.
     *
     * We define an ideal mixture as a fluid phase where the fugacity
     * coefficients of all components times the pressure of the phase
     * are indepent on the fluid composition. This assumtion is true
     * if Henry's law and Raoult's law apply. If you are unsure what
     * this function should return, it is safe to return false. The
     * only damage done will be (slightly) increased computation times
     * in some cases.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isIdealMixture(int phaseIdx = liquidPhaseIdx)
    {
        assert(phaseIdx == liquidPhaseIdx);
        return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be compressible.
     *
     * Compressible means that the partial derivative of the density
     * to the fluid pressure is always larger than zero.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isCompressible(int phaseIdx = liquidPhaseIdx)
    {
        assert(phaseIdx == liquidPhaseIdx);
        return H2O::liquidIsCompressible();
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal gas.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isIdealGas(int phaseIdx = liquidPhaseIdx)
    {
        assert(phaseIdx == liquidPhaseIdx);
        return false; /*we're a liquid!*/
    }

    /****************************************
     * Component related static parameters
     ****************************************/

    /*!
     * \brief Return the human readable name of a component
     * \param compIdx The index of the component to consider
     */
    static std::string componentName(int compIdx)
    {
        static std::string name[] = {
            H2O::name(),
            Caesium137::name(),
            Iod129::name(),
            Strontium90::name(),
            Uran235::name()
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return name[compIdx];
    }

    /*!
     * \brief Returns true if the component is radioactive
     * \param compIdx The index of the component to consider
     */
    static bool isRadioactive(int compIdx)
    {
        static constexpr bool iR[] = {
            false,
            Caesium137::isRadioactive(),
            Iod129::isRadioactive(),
            Strontium90::isRadioactive(),
            Uran235::isRadioactive()
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return iR[compIdx];
    }

    /*!
     * \brief Return the molar mass of a component in \f$\mathrm{[kg/mol]}\f$.
     * \param compIdx The index of the component to consider
     */
    static Scalar molarMass(int compIdx)
    {
        static const Scalar M[] = {
            H2O::molarMass(),
            Caesium137::molarMass(),
            Iod129::molarMass(),
            Strontium90::molarMass(),
            Uran235::molarMass()
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return M[compIdx];
    }

    /*!
     * \brief Return the half-life of a radioactive component in \f$\mathrm{[s]}\f$.
     * \param compIdx The index of the component to consider
     */
    static Scalar halfLife(int compIdx)
    {
        static const Scalar HL[] = {
            std::numeric_limits<Scalar>::infinity(), //infinity, but shouldnt needed
            Caesium137::halfLife(),
            Iod129::halfLife(),
            Strontium90::halfLife(),
            Uran235::halfLife()
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return HL[compIdx];
    }

    /*!
     * \brief Return the daughter component of the radioactive decay
     * \param compIdx The index of the component to consider
     */
    static std::string decayDaughter(int compIdx)
    {
        static std::string dname[] = {
            "",
            Caesium137::decayDaughter(),
            Iod129::decayDaughter(),
            Strontium90::decayDaughter(),
            Uran235::decayDaughter()
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return dname[compIdx];
    }

    static Scalar kdValue(int compIdx)
    {
        static const Scalar kd[] = {
            0.0,
            Caesium137::kdValue(),
            Iod129::kdValue(),
            Strontium90::kdValue(),
            Uran235::kdValue()          
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return kd[compIdx];
    }

    /*!
     * \brief Return the specific activity of a radioactive componet in \f$\mathrm{[Bq/kg]}\f$.
     * \param compIdx The index of the component to consider
     */
    static Scalar specificActivity(int compIdx)
    {
        static const Scalar A[] = {
            0.0,
            Caesium137::specificActivity(),
            Iod129::specificActivity(),
            Strontium90::specificActivity(),
            Uran235::specificActivity()         
        };
        assert(0 <= compIdx && compIdx < numComponents);
        return A[compIdx];
    }

    /*!
     * \brief Calculation of decay rate of a component
     * Used is the usual radioactive decay equation N = N*exp(-t/lambda).
     * Here the derivation of this equation is used, also possible are two other formulations:
     * (1 - std::pow(2,(-timeStepSize/halfLife)))/timeStepSize;
     * (1 - exp(-log(2)/halfLife * timeStepSize))/timeStepSize
     * \param compIdx The index of the component to consider
     */
    static Scalar decayRate(int compIdx, const Scalar halfLife)
    {
        if(compIdx == H2OIdx)
            return 0.0;
        else
            return log(2)/halfLife;
    }

    /****************************************
     * thermodynamic relations
     ****************************************/
    /*!
     * \brief Initialize the fluid system's static parameters generically
     * \note If a tabulated H2O component is used, we do our best to create
     *       tables that always work.
     */
     static void init()
     {
         init(/*tempMin=*/273.15,
              /*tempMax=*/623.15,
              /*numTemp=*/100,
              /*pMin=*/-10.,
              /*pMax=*/20e6,
              /*numP=*/200);
     }

    /*!
     * \brief Initialize the fluid system's static parameters using
     *        problem specific temperature and pressure ranges
     *
     * \param tempMin The minimum temperature used for tabulation of water [K]
     * \param tempMax The maximum temperature used for tabulation of water [K]
     * \param nTemp The number of ticks on the temperature axis of the  table of water
     * \param pressMin The minimum pressure used for tabulation of water [Pa]
     * \param pressMax The maximum pressure used for tabulation of water [Pa]
     * \param nPress The number of ticks on the pressure axis of the  table of water
     */
    static void init(Scalar tempMin, Scalar tempMax, unsigned nTemp,
                     Scalar pressMin, Scalar pressMax, unsigned nPress)
    {

        if (H2O::isTabulated)
        {
            std::cout << "Initializing tables for the H2O fluid properties ("
                      << nTemp*nPress
                      << " entries).\n";

            H2O::init(tempMin, tempMax, nTemp, pressMin, pressMax, nPress);
        }
    }

    using Base::density;
    /*!
     * \brief Return the phase density [kg/m^3].
     * \note The density is compuated as a function temperature and pressure
     */
    template <class FluidState>
    static Scalar density(const FluidState& fluidState, int phaseIdx = liquidPhaseIdx)
    {
        return H2O::liquidDensity(fluidState.temperature(phaseIdx),
                       fluidState.pressure(phaseIdx));
    }

    using Base::fugacityCoefficient;
    /*!
     * \copybrief Base::fugacityCoefficient
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component to consider
     */
    template <class FluidState>
    static Scalar fugacityCoefficient(const FluidState &fluidState,
                                      int phaseIdx,
                                      int compIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);
        assert(0 <= compIdx  && compIdx < numComponents);

        if (phaseIdx == compIdx)
            // We could calculate the real fugacity coefficient of
            // the component in the fluid. Probably that's not worth
            // the effort, since the fugacity coefficient of the other
            // component is infinite anyway...
            return 1.0;
        return std::numeric_limits<Scalar>::infinity();
    }

    using Base::viscosity;
    /*!
     * \brief Return the viscosity of the phase.
     */
    template <class FluidState>
    static Scalar viscosity(const FluidState& fluidState, int phaseIdx = liquidPhaseIdx)
    {
        return H2O::liquidViscosity(fluidState.temperature(phaseIdx),
                         fluidState.pressure(phaseIdx)); // [Pa·s]
    }

    /*!
     * \brief Vapor pressure of a component \f$\mathrm{[Pa]}\f$.
     *
     * \param fluidState The fluid state
     * \param compIdx The index of the component to consider
     */
    template <class FluidState>
    static Scalar vaporPressure(const FluidState& fluidState, int compIdx)
    {
        if (compIdx == H2OIdx)
        {
            // simplified version of Eq 2.29 in Vishal Jambhekar's Promo
            const Scalar temperature = fluidState.temperature(H2OIdx);
            return H2O::vaporPressure(temperature)/fluidState.massFraction(phase0Idx, H2OIdx);
        }
        else if (compIdx != H2OIdx)
            DUNE_THROW(Dune::NotImplemented, "RadIon::vaporPressure(t)");
        else
            DUNE_THROW(Dune::NotImplemented, "Invalid component index " << compIdx);
    }

    /*!
     * \brief Returns the specific enthalpy \f$\mathrm{[J/kg]}\f$ of a component in a specific phase
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param componentIdx The index of the component to consider
     *
     */
    template <class FluidState>
    static Scalar componentEnthalpy(const FluidState &fluidState,
                                    int phaseIdx,
                                    int componentIdx)
    {
        const Scalar T = fluidState.temperature(liquidPhaseIdx);
        const Scalar p = fluidState.pressure(liquidPhaseIdx);

        if (phaseIdx == liquidPhaseIdx)
        {
            if (componentIdx == H2OIdx)
                return H2O::liquidEnthalpy(T, p);
            else if (componentIdx != H2OIdx)
                DUNE_THROW(Dune::NotImplemented, "The component enthalpy for RadIon is not implemented.");
            DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << componentIdx);
        }
        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }

    using Base::molarDensity;
    /*!
     * \brief The molar density \f$\rho_{mol,\alpha}\f$ of
     *        the fluid phase \f$\alpha\f$ in \f$\mathrm{[mol/m^3]}\f$
     *
     * The molar density for the simple relation is defined by the
     * mass density \f$\rho_\alpha\f$ and the molar mass of the main component \f$M_\kappa\f$:
     *
     * \f[\rho_{mol,\alpha} = \frac{\rho_\alpha}{M_\kappa} \;.\f]
     */
    template <class FluidState>
    static Scalar molarDensity(const FluidState& fluidState, int phaseIdx = liquidPhaseIdx)
    {
        return density(fluidState, phaseIdx) /*H2O::molarMass();*/ /fluidState.averageMolarMass(phaseIdx);
    }

    using Base::diffusionCoefficient;
    /*!
     * \brief Returns the diffusion coefficient \f$\mathrm{[-]}\f$
     *        of a component in a phase.
     */
    template <class FluidState>
    static Scalar diffusionCoefficient(const FluidState& fluidState, int phaseIdx, int compIdx)
    {
        DUNE_THROW(Dune::NotImplemented, "FluidSystems::H2ORad::diffusionCoefficient()");
    }

    using Base::binaryDiffusionCoefficient;
    /*!
     * \brief Given a phase's composition, temperature and pressure,
     *        return the binary diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for components
     *        \f$\mathrm{i}\f$ and \f$\mathrm{j}\f$ in this phase.
     * \param fluidState The fluid state
     * \param phaseIdx Index of the fluid phase
     * \param compIIdx Index of the component i
     * \param compJIdx Index of the component j
     */
    template <class FluidState>
    static Scalar binaryDiffusionCoefficient(const FluidState& fluidState,
                                             int phaseIdx,
                                             int compIIdx,
                                             int compJIdx)
    {
        if (phaseIdx == liquidPhaseIdx)
        {
            if (compIIdx > compJIdx)
            {
                using std::swap;
                swap(compIIdx, compJIdx);
            }
            //! \todo TODO implement binary coefficients
            // http://webserver.dmt.upm.es/~isidoro/dat1/Mass%20diffusivity%20data.htm
            // The link above was given as a reference in brine_air fluid system.
            // Doesn't work anymore though...
            if (compJIdx != H2OIdx)
                return 0.12e-9;
            else
                DUNE_THROW(Dune::NotImplemented, "Binary diffusion coefficient of components "
                                                 << compIIdx << " and " << compJIdx
                                                 << " in phase " << phaseIdx);
         }

         DUNE_THROW(Dune::InvalidStateException, "Invalid phase index: " << phaseIdx);
    }

};

} // end namespace FluidSystems
} // end namespace Dumux

#endif
