// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief A class for the radioactive Caesium137 component properties
 */
#ifndef DUMUX_CAESIUM_HH
#define DUMUX_CAESIUM_HH

#include <dumux/material/components/base.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief A class for the radioactive Caesium137 component properties.
 */
template <class Scalar>
class Caesium137
: public Components::Base<Scalar, Caesium137<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the Caesium ion.
     */
    static std::string name()
    { return "Cs137"; }

    /*!
     * \brief Returns true if the component is radioactive
     */
    static constexpr bool isRadioactive()
    { return true; }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of the Caesium ion.
     */
    static constexpr Scalar molarMass()
    { return 137e-3; } // kg/mol

    /*!
     * \brief The half-life of the Caesium ion in \f$\mathrm{[s]}\f$.
     */
    static constexpr Scalar halfLife()
    { return 9.514e8; }

    /*!
     * \brief The kd-value of the Caesium ion in \f$\mathrm{[m^3/kg]}\f$.
     */
    static constexpr Scalar kdValue()
    { return 0.001; }

    /*!
     * \brief Return the theoretical specific activity of a radioactive component in \f$\mathrm{[Bq/kg]}\f$,
     * calculated with the half-life.
     */
    static constexpr Scalar specificActivity()
    { return log(2) / halfLife() * 6.02214076e23 / molarMass(); } 

    /*!
     * \brief The daughter of the radioactive decay.
     */
    static std::string decayDaughter()
    { return "stable"; }
};

} // end namespace Components
} // end namespace Dumux

#endif
