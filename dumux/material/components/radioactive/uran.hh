// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief A class for different radioactive Uran component properties
 */
#ifndef DUMUX_URAN_HH
#define DUMUX_URAN_HH

#include <dumux/material/components/base.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief A class for different radioactive Uran component properties.
 */
template <class Scalar>
class Uran238
: public Components::Base<Scalar, Uran238<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the Uran ion.
     */
    static std::string name()
    { return "U238"; }

    /*!
     * \brief Returns true if the component is radioactive
     */
    static constexpr bool isRadioactive()
    { return true; }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of the Uran ion.
     */
    static constexpr Scalar molarMass()
    { return 238e-3; } // kg/mol

    /*!
     * \brief The half-life of the Uran ion in \f$\mathrm{[s]}\f$.
     */
    static constexpr Scalar halfLife()
    { return 1.409e17; }

    /*!
     * \brief The kd-value of the Uran ion in \f$\mathrm{[m^3/kg]}\f$.
     */
    static constexpr Scalar kdValue()
    { return 0.01; } //m^3/kg

    /*!
     * \brief Return the theoretical specific activity of a radioactive component in \f$\mathrm{[Bq/kg]}\f$,
     * calculated with the half-life.
     */
    static constexpr Scalar specificActivity()
    { return log(2) / halfLife() * 6.02214076e23 / molarMass(); } 

    /*!
     * \brief The daughter of the radioactive decay.
     */
    static std::string decayDaughter()
    { return "stable"; }
};

template <class Scalar>
class Uran235
: public Components::Uran238<Scalar>
{
public:
    /*!
     * \brief A human readable name for the Uran ion.
     */
    static std::string name()
    { return "U235"; }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of the Uran ion.
     */
    static constexpr Scalar molarMass()
    { return 235e-3; } // kg/mol

    /*!
     * \brief The half-life of the Uran ion in \f$\mathrm{[s]}\f$.
     */
    static constexpr Scalar halfLife()
    { return 2.220e18; }

    /*!
     * \brief The daughter of the radioactive decay.
     */
    static std::string decayDaughter()
    { return "stable"; }
};

template <class Scalar>
class Uran234
: public Components::Uran238<Scalar>
{
public:
    /*!
     * \brief A human readable name for the Uran ion.
     */
    static std::string name()
    { return "U234"; }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of the Uran ion.
     */
    static constexpr Scalar molarMass()
    { return 234e-3; } // kg/mol

    /*!
     * \brief The half-life of the Uran ion in \f$\mathrm{[s]}\f$.
     */
    static constexpr Scalar halfLife()
    { return 7.74e12; }

    /*!
     * \brief The daughter of the radioactive decay.
     */
    static std::string decayDaughter()
    { return "stable"; }
};

} // end namespace Components
} // end namespace Dumux

#endif
