SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:  

Archontoula Valsamidou
Numerical studies on the modeling of macropores 
Master's Thesis, 2021
University of Stuttgart

Installation
============

The easiest way to install this module and its dependencies is to create a new directory

```
mkdir DUMUX-Macropores && cd DUMUX-Macropores
```

and download the install script

[installvalsamidou2021a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/valsamidou2021a/-/blob/master/installvalsamidou2021a.sh)

to that folder and run the script with

```
chmod u+x installvalsamidou2021a.sh
./installvalsamidou2021a.sh
```

Installation with Docker 
========================

Create a new folder in your favourite location and change into it

```bash
mkdir DUMUX-Macropores
cd DUMUX-Macropores
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/valsamidou2021a/-/raw/master/docker_valsamidou2021a.sh
```
Open the Docker Container
````bash
bash docker_valsamidou2021a.sh open
````

Reproducing the results
=======================

After the script has run successfully, you may build the program executables

```
cd valsamidou2021a/build-cmake/appl/tracer/tracer2p
make test_tracer2p_tpfa
make test_tracer2p_forchheimer1_tpfa
make test_tracer2p_forchheimer2_tpfa
```

and run them, e.g., with

```
./test_tracer2p_tpfa
```
