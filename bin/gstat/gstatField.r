library(sp)
library(gstat)
# unconditional simulation on a 100 x 100 grid
xy <- expand.grid(1:50, 1:50)
names(xy) <- c("x","y")
gridded(xy) = ~x+y
g.dummy <- gstat(formula = z~1, dummy = TRUE, beta = 0, model = vgm(2, "Mat", 2, kappa=3, anis = c(90,0.25)), nmax = 100) # for speed -- 10 is too small!!
yy <- predict(g.dummy, xy, nsim = 1)
# show all four:
spplot(yy)
# write text-file
write.table(yy, "permeability3.dat", append = FALSE, sep = " ", dec = ".",
            row.names = FALSE, col.names = TRUE)