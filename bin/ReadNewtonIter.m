close all;
clear all;

simrun_names = {'tracer2p_', 'tracerri_'};
scheme_names = {'1_f1.0', '2_f1.0' , '3_f1.0', 'lenscl1_f1.0', 'lenscl2_f1.0', 'lenscl3_f1.0', 'lensop1_f1.0', 'lensop2_f1.0', 'lensop3_f1.0'};

numTests = max(size(simrun_names));
numSchemes = max(size(scheme_names));

fileID = fopen('NewtonIterOutput.txt','w');
for i=1:numTests
    for j=1:numSchemes
        name = strjoin([simrun_names(i) scheme_names(j) '.out'],'');
        
        fid = fopen(name, 'r');
        
        stepIndex = 1; newtonIt = 0; numFailed = 0; totalTime = 0;
        line = fgetl(fid);
        while feof(fid) == 0

            if (strfind(line, 'Newton iteration') ~= 0)
                if (newtonIt == 0)
                    dataGlobal(stepIndex,1) = 0;
                    timeData(stepIndex,1:2) = 0;
                end
                newtonIt = newtonIt + 1;
            end
            if (strfind(line, 'Assemble/solve') ~= 0)
                lineNoWhiteSpace = sscanf(line,'%s');
                timesVec = sscanf(lineNoWhiteSpace,'Assemble/solve/update time: %f(%f%%)/%f(%f%%)/%f(%f%%)',[6,inf])';
                totalTime = totalTime + timesVec(1) + timesVec(3) + timesVec(5);
            end
            
            if (strfind(line, 'Wall') ~= 0)
                lineNoWhiteSpace = sscanf(line,'%s');
                timesVec = sscanf(lineNoWhiteSpace,'[%f%%]Timestep%fdonein%fseconds.Wallclocktime:%f,time:%f,timestepsize:%f',[6,inf]);
                timeData(stepIndex,:) =  timeData(stepIndex,:) + timesVec(end-1:end).';
                dataGlobal(stepIndex,1) = newtonIt;
                newtonIt = 0;
                stepIndex = stepIndex + 1;
            end
            
            
            if (strfind(line, 'Retrying') ~= 0)
                timeData(stepIndex,1:end) = 0;
                dataGlobal(stepIndex,1) = 0;
                newtonIt = 0;
                numFailed = numFailed + 1;
            end
            line = fgetl(fid);
        end
        
        if(newtonIt > 0)
            globalSteps = stepIndex;
        else
            globalSteps = stepIndex - 1;
        end
        
        N1 = name;
        F1 = numFailed;
        GT1 = globalSteps;
        NI1 = sum(dataGlobal(:,1));
        T1 = totalTime;
        
        formatSpec = '###############\n %s \n Total time:\n %6.2f\n No. global timesteps:\n %5.0f\n No. Newton iterations:\n %6.0f\n No. failed timesteps:\n %4.0f\n \n';
        fprintf(fileID, formatSpec, N1, T1, GT1, NI1, F1);

        clear dataGlobal;
        clear timeData
    end
end
fclose(fileID);
