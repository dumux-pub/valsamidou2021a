// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPTests
 * \brief The spatial params for the flow problem of a radioactive 2p test.
 */

#ifndef DUMUX_TWOPFLOW_TEST_SPATIAL_PARAMS_HH
#define DUMUX_TWOPFLOW_TEST_SPATIAL_PARAMS_HH

#include <random>
#include <dumux/porousmediumflow/properties.hh>
#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/material/spatialparams/gstatrandomfield.hh>


namespace Dumux {

/*!
 * \ingroup TwoPTests
 * \brief The spatial params for the flow problem of a radioactive 2p test.
 */
template<class GridGeometry, class Scalar>
class TwoPFlowTestSpatialParams
: public FVSpatialParams<GridGeometry, Scalar, TwoPFlowTestSpatialParams<GridGeometry, Scalar>>
{
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ThisType = TwoPFlowTestSpatialParams<GridGeometry, Scalar>;
    using ParentType = FVSpatialParams<GridGeometry, Scalar, ThisType>;
    
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using EffectiveLaw = RegularizedBrooksCorey<Scalar>;

public:
    using MaterialLaw = EffToAbsLaw<EffectiveLaw>;
    using MaterialLawParams = typename MaterialLaw::Params;
    using PermeabilityType = Scalar;
   

/*    // multiphase adapter setup
        std::array<Scalar, 2> relativePermeability_; //!< Effective relative permeability within the control volume
        std::array<Scalar, 2> effectivePermeability_;
        using MPAdapter = MPAdapter<MaterialLaw,2>; // for the two phases
        MPAdapter::relativePermeabilities(relativePermeability_, MaterialLawParams, volVars, wPhaseIdx);
        
        template <class ContainerT, class VolumeVariables>
        static void relativePermeabilities(ContainerT &values,
                                        const Params &params,
                                        const VolumeVariables &volVars,// removed fluidstate, added volume variables
                                        int wPhaseIdx)
     {
         assert(values.size() == 2);
         const int nPhaseIdx = 1 - wPhaseIdx;
         values[wPhaseIdx] = MaterialLaw::krw(params, volVars.saturation(wPhaseIdx));
         values[nPhaseIdx] = MaterialLaw::krn(params, volVars.saturation(wPhaseIdx));
     }
  */  //    
    
    TwoPFlowTestSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry), K_(gridGeometry->gridView().size(0), 0.0), MaterialParams_(gridGeometry->gridView().size(0))
    {
        PeInit_ = getParam<Scalar>("SpatialParams.Pe", 1e3);
        permeability_ = getParam<Scalar>("SpatialParams.K", 1e-11);
        bool gstatRandomField = getParam<bool>("Gstat.gstatRandomField", false);
        bool lens = getParam<bool>("SpatialParams.lens", false); // false if not defined, else from input file 
        std::vector<double> lens1 = getParam<std::vector<double>>("SpatialParams.Lens1");
        lens1Perm = getParam<Scalar>("SpatialParams.Lens1Perm", 1e-10);
        std::vector<double> lens2 = getParam<std::vector<double>>("SpatialParams.Lens2");
        lens2Perm = getParam<Scalar>("SpatialParams.Lens2Perm", 1e-10);
        if (gstatRandomField == true)
        {
            bool newPermField = getParam<bool>("Gstat.newPermField", true); //only read or create new
            initRandomField(*gridGeometry, newPermField);
        }
        else
        {
            //reading permeability from an external created file
            std::string permDataFile = getParam<std::string>("SpatialParams.permFile");
            Scalar permScalingFactor = getParam<Scalar>("SpatialParams.permScalingFactor", 1.0);
            
            std::ifstream permFile(permDataFile);
            if (!permFile.good())
            {
                DUNE_THROW(Dune::IOError, "Reading from file: "
                         << permDataFile << " failed." << std::endl);
            }
            std::string line;
            std::getline(permFile, line);

            Scalar trash, dataValue;
            for (const auto& element : elements(this->gridGeometry().gridView()))
            {
                std::getline(permFile, line);
                std::istringstream curLine(line);
                auto elementIdx_ = this->gridGeometry().elementMapper().index(element);
            
                if (dim == 1)
                    curLine >> trash >> dataValue;  
                else if (dim == 2)
                    curLine >> trash >> trash >> dataValue;
                else if (dim == 3)
                    curLine >> trash >> trash >> trash >> dataValue;
                else
                    DUNE_THROW(Dune::InvalidStateException, "Invalid dimension " << dim);

                if (lens == true)
                {
                    auto x = element.geometry().center()[0];
                    auto y = element.geometry().center()[1];
                    if ( lens1[0] < x && x < lens1[1] && lens1[2] < y && y < lens1[3])
                        K_[elementIdx_] = lens1Perm;
                    else if ( lens2[0] < x && x < lens2[1] && lens2[2] < y && y < lens2[3])
                        K_[elementIdx_] = lens2Perm;  
                    else
                        K_[elementIdx_] = permeability_ * std::pow(10, (permScalingFactor*dataValue)); 
                }
                else
                    K_[elementIdx_] = permeability_ * std::pow(10, (permScalingFactor*dataValue));

                // Leverett after Saadatpoor, 2009, Effect of capillary heterogeneity
                MaterialParams_[elementIdx_].setSwr(0.1);
                MaterialParams_[elementIdx_].setSnr(0.001);
                MaterialParams_[elementIdx_].setPe(PeInit_* std::sqrt(permeability_ / K_[elementIdx_]));
                MaterialParams_[elementIdx_].setLambda(2);
                
                // effective permeability calculation 
                //effectivePermeability_[elementIdx][0] = relativePermeability_[elementIdx][0]*K_[elementIdx];
                //effectivePermeability_[elementIdx][1] = relativePermeability_[elementIdx][1]*K_[elementIdx];
            }
        }
        // Bottom without capillary pressure
        MaterialParamsBottom_.setSwr(0.1);
        MaterialParamsBottom_.setSnr(0.001);
        MaterialParamsBottom_.setPe(0.0);
        MaterialParamsBottom_.setLambda(2);
         
    }

    void initRandomField(const GridGeometry& gg, bool createNew)
    {
        const auto& gridView = gg.gridView();
        const auto& elementMapper = gg.elementMapper();
        const auto gStatControlFile = getParam<std::string>("Gstat.ControlFile");
        const auto gStatInputFile = getParam<std::string>("Gstat.InputFile");
        const auto outputFilePrefix = getParam<std::string>("Gstat.OutputFilePrefix");

        // create random permeability object
        using RandomField = GstatRandomField<GridView, Scalar>;
        RandomField randomPermeabilityField(gridView, elementMapper);
        randomPermeabilityField.create(gStatControlFile,
                                       gStatInputFile,
                                       outputFilePrefix + ".dat",
                                       RandomField::FieldType::log10,
                                       createNew);
        K_.resize(gridView.size(dim), 0.0);

        // copy vector from the temporary gstat object
        K_ = randomPermeabilityField.data();
    }

    //! get the permeability field for output
    const std::vector<Scalar>& getPermField() const
    {return K_; }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *        In this test, we use element-wise distributed permeabilities.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return The permeability
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    { return K_[scv.dofIndex()]; }

     /*!
     * \brief Returns the tortuosity \f$[-]\f$
     *
     * \param globalPos The global position
     */
     Scalar tortuosityAtPos(const GlobalPosition& globalPos) const
    { return 1/porosityAtPos(globalPos); }
    
    
    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param globalPos The global position
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 0.35; } 
    
    const Scalar tortuosity_ = 1/0.35;
    const Scalar porosity_ = 0.35; 
    
    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law.
     *
     * In this test, we use element-wise distributed material parameters.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return The material parameters object
     */
    template<class ElementSolution>
    const MaterialLawParams& materialLawParams(const Element& element,
                                               const SubControlVolume& scv,
                                               const ElementSolution& elemSol) const
    {
        const auto& globalPos = scv.dofPosition();

        if (globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_)
            return MaterialParamsBottom_;
        else
            return MaterialParams_[scv.dofIndex()];
    }

    const MaterialLawParams& materialLawParams(const Element& element) const
    {
        const auto& element_ = this->gridGeometry().elementMapper().index(element);
        const auto& globalPos = element.geometry().center();

        if (globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_)
            return MaterialParamsBottom_;
        else
            return MaterialParams_[element_];
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \param globalPos The global position
     * \return The wetting phase index
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; }

/*	// custom functions - forchheimer coefficient calculation, according to Zhang (2013)
	//template<class RelativePermeability>
	Scalar forchCoeff(const SubControlVolumeFace& scvf,
 					  const auto& relPerm,
					  const Element& element, 
					  int wPhaseIdx
					  ) const
	
     	{
         	return relPerm[0][0];//0.55;//forchCoeffDefault_;
         	
     	}
*/

private:

    
    Scalar permeability_;
    Scalar PeInit_;
    Scalar lens1Perm;
    Scalar lens2Perm;
    
    std::vector<Scalar> K_;
    std::vector<MaterialLawParams> MaterialParams_;

    MaterialLawParams MaterialParamsBottom_;

    static constexpr Scalar eps_ = 1.5e-7;
};

} // end namespace Dumux

#endif
