// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPTests
 * \brief The properties for the flow problem of a radioactive 2p test.
 */
#ifndef DUMUX_TWOPFLOW_TEST_PROBLEM_HH
#define DUMUX_TWOPFLOW_TEST_PROBLEM_HH

#include <dune/grid/yaspgrid.hh>
#include <iostream>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>
#include <dumux/porousmediumflow/2p/incompressiblelocalresidual.hh>

#include <dumux/io/gnuplotinterface.hh>
#include <dumux/io/plotmateriallaw.hh>

#include "spatialparams_2p.hh"

#ifndef ENABLEINTERFACESOLVER
#define ENABLEINTERFACESOLVER 0
#endif

#if FORCHHEIMER1
#include <dumux/flux/forchheimerslaw_cf.hh> // forchheimer solution with constant coefficient
#elif FORCHHEIMER2
#include <dumux/flux/forchheimerslaw_beta.hh> // forchheimer solution with calculated coefficient
#endif

namespace Dumux {
// forward declarations
template<class TypeTag> class TwoPFlowTestProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct TwoPFlow { using InheritsFrom = std::tuple<TwoP>; };
struct TwoPFlowTpfa { using InheritsFrom = std::tuple<TwoPFlow, CCTpfaModel>; };
struct TwoPFlowMpfa { using InheritsFrom = std::tuple<TwoPFlow, CCMpfaModel>; };
struct TwoPFlowBox { using InheritsFrom = std::tuple<TwoPFlow, BoxModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPFlow> { using type = Dune::YaspGrid<2>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPFlow> { using type = TwoPFlowTestProblem<TypeTag>; };

// the local residual containing the analytic derivative methods
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::TwoPFlow> { using type = TwoPIncompressibleLocalResidual<TypeTag>; };

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPFlow>
{
    using Scalar = GetPropType<TypeTag, Scalar>;
    using type = FluidSystems::H2OAir<Scalar, Components::SimpleH2O<Scalar> >;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPFlow>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = TwoPFlowTestSpatialParams<GridGeometry, Scalar>;
};

#ifdef FORCHHEIMER1  // both solutions should be defined here
// Specialize the advection type for this type tag
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::TwoPFlow>
{using type = ForchheimersLaw<TypeTag>; };
#elif FORCHHEIMER2
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::TwoPFlow>
{using type = ForchheimersLaw<TypeTag>; }; 
#endif

// Enable caching
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::TwoPFlow> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::TwoPFlow> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::TwoPFlow> { static constexpr bool value = false; };

// Maybe enable the box-interface solver
template<class TypeTag>
struct EnableBoxInterfaceSolver<TypeTag, TTag::TwoPFlow> { static constexpr bool value = ENABLEINTERFACESOLVER; };
} // end namespace Properties

/*!
 * \ingroup TwoPTests
 * \brief The properties for the flow problem of a radioactive 2p test.
 */
template<class TypeTag>
class TwoPFlowTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using MaterialLaw = typename ParentType::SpatialParams::MaterialLaw;
    using MaterialLawParams = typename ParentType::SpatialParams::MaterialLawParams;
    enum {
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::saturationIdx,
        conti0EqIdx = Indices::conti0EqIdx + FluidSystem::comp0Idx,
        conti1EqIdx = Indices::conti0EqIdx + FluidSystem::comp1Idx,
        waterPhaseIdx = FluidSystem::phase0Idx,
        gasPhaseIdx = FluidSystem::phase1Idx
    };

public:
    TwoPFlowTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry) {
         injectionMass_ = getParam<Scalar>("Problem.InjectionMass", -0.0);
    }
	
	Scalar forchConst(const SubControlVolumeFace scvf) const
    
    {	
    	Scalar forchConst = 0.55; // units
    	//std::cout<<forchConst<<std::endl;
     	return forchConst;
    }
	
    Scalar forchCoeff(const ElementVolumeVariables elemVolVars,
                      const SubControlVolumeFace scvf,
                      const MaterialLawParams materialLawParams,
                      int phaseIdx) const 
    {
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];
        //auto fvGeometry = localView(this->gridGeometry());
        //const auto element = fvGeometry.gridGeometry().element(scvf.insideScvIdx()); 
        //auto elementIdx_ = this->gridGeometry().elementMapper().index(element);
        
        auto satReg = std::max(volVars.saturation(phaseIdx),1e-4);
        //std::cout << "Sat of " << phaseIdx << ": " << sat << std::endl;
        
        // relative permeability - depending on phase 
        //const Scalar kr = (phaseIdx==waterPhaseIdx) ? MaterialLaw::krw(this->spatialParams().materialLawParams(element), satReg) : MaterialLaw::krn(this->spatialParams().materialLawParams(element), satReg);
        //auto krReg = std::max(kr,1e-4);
        //std::cout << "kr of " << phaseIdx << ": " << kr << std::endl;
        
        //parameter for the calculation of forchCoeff  
		const Scalar C_beta = 1.52e-4; //[m] (Zhang, 2013)
		
		// tortuosity from the spatial parameters
		const Scalar tau = this->spatialParams().tortuosity_;
		
		// porosity from the spatial parameters
		const Scalar phi = this->spatialParams().porosity_;
		
	    // calculation of the coefficient
	    Scalar forchCoeff = 0.55; 
		//if (volVars.saturation(phaseIdx) > 0 && volVars.saturation(phaseIdx) < 1 && this->spatialParams().getPermField()[elementIdx_] > 0 && this->spatialParams().getPermField()[elementIdx_] < 1)
		
		forchCoeff = (C_beta*tau)/(phi*satReg); 
        //std::cout<<forchCoeff<<std::endl;
        
        return forchCoeff; 
    }
	
    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        if (onLowerBoundary_(globalPos))
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

    template<class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        const auto& globalPos = scvf.ipGlobal();
        //const auto& volVars = elemVolVars[scvf.insideScvIdx()];
        if (onUpperBoundary_(globalPos))
        {
            values[conti0EqIdx] = injectionMass_;
        }
        /*if (onUpperBoundary_(globalPos))
        {
            const auto& volVars = elemVolVars[scvf.insideScvIdx()];
            const Scalar pW = volVars.pressure(waterPhaseIdx);
            const Scalar pN = volVars.pressure(gasPhaseIdx);
            const Scalar satW = volVars.saturation(waterPhaseIdx);
            const Scalar satN = volVars.saturation(gasPhaseIdx);
            const Scalar kf = 0.00571033287478;
            values[conti0EqIdx] = satW * (pW-1e5) * kf;
            if (pN>1.e5) 
            { 
                values[conti1EqIdx] = satN * (pN-1.e5) * kf; 
            }
            values[conti0EqIdx] = injectionMass_;
        }*/
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolumeFace &scvf) const
    {
        const auto& globalPos = scvf.ipGlobal();
        PrimaryVariables values;
        const Scalar sw = 0.1;
        const Scalar pc = MaterialLaw::pc(this->spatialParams().materialLawParams(element), sw);
        if (onLowerBoundary_(globalPos))
        {
            values[pressureIdx] = 1e5 - pc;
            values[saturationIdx] = 1.0 - sw;
        }

        return values;
    }

    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initial(const Element& element) const
    {
        PrimaryVariables values;

        const Scalar sw = 0.1;
        const Scalar pc = MaterialLaw::pc(this->spatialParams().materialLawParams(element), sw);

        values[pressureIdx] = 1e5 - pc;
        values[saturationIdx] = 1.0 - sw;
        return values;
    }

    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
     *
     * This is not specific to the discretization. By default it just
     * throws an exception so it must be overloaded by the problem if
     * no energy equation is used.
     */
    Scalar temperature() const
    {
        return 293.15; // 10°C
    }

    /*!
     * \brief Returns the time for a variable neumann boundary condition.
     *
     */
    void setTime(Scalar time)
    { 
        time_ = time; 
    }


private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_;
    }

    static constexpr Scalar eps_ = 1e-6;

    Scalar time_;
    Scalar injectionMass_;

    std::vector<Scalar> K_;
    std::vector<Scalar> vtkK_;
};

} // end namespace Dumux

#endif
